@extends('layouts.app')

@section('content')
    <div class="d-flex justify-content-center align-items-center">
        <div class="d-flex flex-column bd-highlight mb-3">
            <div class="p-2 bd-highlight"><img class="profile-image  img-fluid rounded mx-auto d-block" src="/storage/profile_images/{{ $user->profile_image }}" alt="Profile image"></div>
            <div class="p-2 bd-highlight"><h1>{{ $user->name }}</h1></div>
            <div class="p-2 bd-highlight"><h2>Points: {{ $user->points }}</h2></div>
            <div class="p-2 bd-highlight">
                <h1>{{ $user->name }}'s Tournaments</h1>
                @forelse($user->tournaments as $tournament)
                    <a href="/tournaments/{{ $tournament->id }}">{{ $tournament->name }}</a><br>
                @empty
                    <p>{{ $user->name }} has not signed to any tournaments</p>
                @endforelse
            </div>
        </div>
    </div>

@endsection
