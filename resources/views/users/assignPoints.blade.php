@extends('layouts.app')

@section('content')
<div class="mt-2 col-md-12"></div>
    {!! Form::open(['action' => ['UsersController@pointsUpdate', $user->id], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
            <div class="form-group">
            <div class="alert alert-danger" role="alert">
                Points are assigned automatically when setting scores! Use with caution!
            </div>
        <h2>{{ $user->name }}</h2>
            {{Form::label('points', 'Points')}}
            {{Form::number('points', $user->points, ['class' => 'form-control', 'placeholder' => 'Edit points'])}}
            </div>
        {{Form::hidden('_method', 'PUT')}}
        {{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
    {!! Form::close() !!}

@endsection
