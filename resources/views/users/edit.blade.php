@extends('layouts.app')

@section('content')
<div class="mt-2 col-md-12"></div>
    @if(Auth::user()->group === 'admin' && Auth::user()->id !== $user->id)
    <h2>{{ $user->name }}</h2>
    {!! Form::open(['action' => ['UsersController@update', $user->id], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
    <div class="form-group">
    {{Form::label('group', 'Group')}}
    {{Form::select('group', $groups, $user->group)}}
    </div>
    {{Form::hidden('_method', 'PUT')}}
    {{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
    {!! Form::close() !!}

    @else
        @if(Auth::user()->id === $user->id)
        <h1>Edit Your Profile</h1>
        {!! Form::open(['action' => ['UsersController@update', $user->id], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
            <div class="form-group">
            {{Form::label('name', 'Name')}}
            {{Form::text('name', $user->name, ['class' => 'form-control', 'placeholder' => 'Name'])}}
            </div>

            <div class="form-group">
            {{Form::label('email', 'Email')}}
            {{Form::email('email', $user->email, ['class' => 'form-control', 'placeholder' => 'Email'])}}
            </div>

            <div class="form-group">
                {{Form::label('password', 'Password')}}
                {{Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password'])}}
            </div>

            <div class="form-group">
                {{Form::label('password_confirmation', 'Password Confirmation')}}
                {{Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => 'Password Confirmation'])}}
            </div>

            <div class="form-group">
                {{Form::label('profile_image', 'Profile Image')}}
                {{Form::file('profile_image', ['class' => 'form-control-file'])}}
            </div>

            {{Form::hidden('_method', 'PUT')}}
            {{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
        {!! Form::close() !!}
        @endif

        @if(Auth::user()->group === 'manager' && Auth::user()->id !== $user->id)
        {!! Form::open(['action' => ['UsersController@update', $user->id], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
                <div class="form-group">
                {{Form::label('points', 'Points')}}
                {{Form::number('points', $user->points, ['class' => 'form-control', 'placeholder' => 'Points'])}}
                </div>
            {{Form::hidden('_method', 'PUT')}}
            {{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
        {!! Form::close() !!}
        @endif
    @endif

@endsection
