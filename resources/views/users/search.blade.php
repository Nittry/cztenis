@extends('layouts.app')

@section('content')
<div class="mt-2 col-md-12"></div>
<h1>Search Results</h1>
@if(!Auth::guest())
    @if(Auth::user()->group === 'admin')
        @if((count($users) > 0))
            <div class="well">
                <div class="row">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Name</th>
                                <th scope="col">Points</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $key => $user)
                            @if(Auth::user()->id === $user->id)
                                <tr>
                                    <th scope="row">{{++$key}}</th>
                                    <td><a href="/users/{{$user->id}}">{{$user->name}}</a></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            @else
                                @if($user->group === 'admin' || $user->group === 'manager')
                                    <tr>
                                        <th scope="row">{{++$key}}</th>
                                        <td><a href="/users/{{$user->id}}">{{$user->name}}</a></td>
                                        <td></td>
                                        <td><a href="/users/{{$user->id}}/edit">Set group</a></td>
                                        <td></td>
                                    </tr>
                                @else
                                    <tr>
                                        <th scope="row">{{++$key}}</th>
                                        <td><a href="/users/{{$user->id}}">{{$user->name}}</a></td>
                                        <td>{{ $user->points }}</td>
                                        <td><a href="/users/{{$user->id}}/edit">Set group</a></td>
                                        <td><a href="/users/{{$user->id}}/points" class="btn btn-default">Assign points</a></td>
                                    </tr>
                                @endif
                            @endif

                        @endforeach
                </div>
            </div>
        @else
            <p>No users found</p>
        @endif
@else
            @if((count($users) > 0))
                <div class="well">
                    <div class="row">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Points</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $key => $user)
                                <tr>
                                    <th scope="row">{{++$key}}</th>
                                    <td><a href="/users/{{$user->id}}">{{$user->name}}</a></td>
                                    <td>{{ $user->points }}</td>
                                </tr>


                            @endforeach
                    </div>
                </div>
            @else
            <p>No users found</p>
            @endif
    @endif

@else
        @if((count($users) > 0))
            <div class="well">
                <div class="row">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Name</th>
                                <th scope="col">Points</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $key => $user)
                            <tr>
                                <th scope="row">{{++$key}}</th>
                                <td><a href="/users/{{$user->id}}">{{$user->name}}</a></td>
                                <td>{{ $user->points }}</td>
                            </tr>


                        @endforeach
                </div>
            </div>
        @else
        <p>No users found</p>
        @endif
@endif
@endsection
