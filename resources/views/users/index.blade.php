@extends('layouts.app')

@section('content')
<div class="mt-2 col-md-12"></div>
    {!! Form::open(['action' => 'UsersController@search', 'method' => 'GET']) !!}
                <div class="form-group">
                    {{Form::text('search', '', ['class' => 'form-control', 'placeholder' => 'Search by name or points'])}}
                </div>
                {{Form::submit('Search', ['class' => 'btn btn-primary'])}}
    {!! Form::close() !!}
    <br>
@if(!Auth::guest())
    @if(Auth::user()->group === 'admin')
            <h1>All Users</h1>
            @if((count($allUsers) > 0))
                <div class="well">
                    <div class="row">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Points</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($allUsers as $key => $user)
                                @if(Auth::user()->id === $user->id)
                                    <tr>
                                        <th scope="row">{{++$key}}</th>
                                        <td><a href="/users/{{$user->id}}">{{$user->name}}</a></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                @else
                                    @if($user->group === 'admin' || $user->group === 'manager')
                                        <tr>
                                            <th scope="row">{{++$key}}</th>
                                            <td><a href="/users/{{$user->id}}">{{$user->name}}</a></td>
                                            <td></td>
                                            <td><a href="/users/{{$user->id}}/edit">Set group</a></td>
                                            <td></td>
                                        </tr>
                                    @else
                                        <tr>
                                            <th scope="row">{{++$key}}</th>
                                            <td><a href="/users/{{$user->id}}">{{$user->name}}</a></td>
                                            <td>{{ $user->points }}</td>
                                            <td><a href="/users/{{$user->id}}/edit">Set group</a></td>
                                            <td><a href="/users/{{$user->id}}/points" class="btn btn-default">Assign points</a></td>
                                        </tr>
                                    @endif
                                @endif

                            @endforeach
                    </div>
                </div>
            @else
                <p>No users found</p>
            @endif
    @else
        <h1>Leaderboard</h1>
                @if((count($users) > 0))
                    <div class="well">
                        <div class="row">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Name</th>
                                        <th scope="col">Points</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $key => $user)
                                    <tr>
                                        <th scope="row">{{++$key}}</th>
                                        <td><a href="/users/{{$user->id}}">{{$user->name}}</a></td>
                                        <td>{{ $user->points }}</td>
                                    </tr>


                                @endforeach
                        </div>
                    </div>
                @else
                <p>No users found</p>
                @endif
        @endif

@else
    <h1>Leaderboard</h1>
            @if((count($users) > 0))
                <div class="well">
                    <div class="row">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Points</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $key => $user)
                                <tr>
                                    <th scope="row">{{++$key}}</th>
                                    <td><a href="/users/{{$user->id}}">{{$user->name}}</a></td>
                                    <td>{{ $user->points }}</td>
                                </tr>


                            @endforeach
                    </div>
                </div>
            @else
            <p>No users found</p>
            @endif
@endif

@endsection
