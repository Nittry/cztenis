<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <script src="{{ asset('js/app.js') }}"></script>
        <title>CZ Tenis</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
    </head>
    <body>
    @include('inc.navbar')
        <header>
            <div class="container float-left">
                <h1>Welcome to Cztenis website</h1>
                <p>This is not an original cztenis website. If you want to go to the original cztenis website click below</a></p>
                <a href="http://cztenis.cz/" class="btn btn-info" target='_blank'>Original Cztenis website</a>
            </div>
        </header>

        <div class="container text-center">
            <h1>Sign in to the tournaments created by managers or admins!</h1>
            <p>Sign in to the tournaments win them and see yourself rise in our leaderboard</p>

            @if(Auth::guest())
            <p>Create an account now and sign in</p>
            <a class="btn btn-primary" href="/register">Register</a>
            @else
                <p>You can find tournaments here</p>
                <a class="btn btn-primary" href="/tournaments">Sign in to a tournament</a>
            @endif

        </div>
    </body>
</html>
