@extends('layouts.app')

@section('content')
<div class="mt-2 col-md-12"></div>
<h1>Set Scores</h1>
@if((count($users) > 1))
    {!! Form::open(['action' => ['ScoresController@store', $tournament->id], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
    <div class="form-group">
        {{Form::label('winnerLabel', 'Winner')}}
        {{Form::select('winner', $users, null)}}
    </div>
    <div class="form-group">
        {{Form::label('loserLabel', 'Loser')}}
        {{Form::select('loser', $users, null)}}
    </div>
    <div class="form-group">
        {{Form::label('scoreLabel', 'Score')}}
        {{Form::text('score', '' ,['class' => 'form-control', 'placeholder' => '6-0 6-0'])}}
    </div>
    {{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
    {!! Form::close() !!}
@else
    <p>Not enough users logged in to this tournament yet to create scores</p>
@endif

@endsection
