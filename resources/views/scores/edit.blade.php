@extends('layouts.app')

@section('content')
<div class="mt-2 col-md-12"></div>
<h1>Set Scores</h1>
{!! Form::open(['action' => ['ScoresController@update', $tournament->id, $score_id], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
        <div class="form-group">
          {{Form::label('winnerLabel', 'Winner')}}
          {{Form::select('winner', $users, $scores[0]->winner_id)}}
        </div>
        <div class="form-group">
            {{Form::label('loserLabel', 'Loser')}}
            {{Form::select('loser', $users, $scores[0]->loser_id)}}
          </div>
        <div class="form-group">
          {{Form::label('scoreLabel', 'Score')}}
          {{Form::text('score', $scores[0]->score ,['class' => 'form-control'])}}
        </div>
        {{Form::hidden('_method', 'PUT')}}
        {{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
{!! Form::close() !!}

@endsection
