@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="d-flex align-content-start flex-wrap">
                    {{-- Links for everyone --}}
                    <div class="p-2 bd-highlight"><a href="{{ route('users.edit', Auth::user()->id) }}" class="btn btn-primary">Update your Profile</a></div>

                    {{-- Links for users only --}}
                    @if(Auth::user()->group === 'user')
                    <div class="p-2 bd-highlight"><a href="/tournaments" class="btn btn-primary">Sign to a tournament</a></div>
                    @endif

                    {{-- Links for managers --}}
                    @if(Auth::user()->group === 'manager')
                    <div class="p-2 bd-highlight"><a href="/tournaments/create" class="btn btn-primary">Create tournament</a></div>
                    <div class="p-2 bd-highlight"><a href="/users" class="btn btn-primary">Assign points manually</a></div>
                    @endif
                    {{-- Links for admins --}}
                    @if(Auth::user()->group === 'admin')
                        <div class="p-2 bd-highlight"><a href="/tournaments/create" class="btn btn-primary">Create tournament</a></div>
                        <div class="p-2 bd-highlight"><a href="/users" class="btn btn-primary">Assign points manually</a></div>
                        <div class="p-2 bd-highlight"><a href="/users" class="btn btn-primary">Set group for users</a></div>
                    @endif
                    </div>
                    <hr>
                    @if(Auth::user()->group === 'user')
                    <div class="d-flex justify-content-center align-items-center">
                        <div class="d-flex flex-column bd-highlight mb-3">
                            <div class="p-2 bd-highlight"><img class="profile-image  img-fluid rounded mx-auto d-block" src="/storage/profile_images/{{ $user->profile_image }}" alt="Profile image"></div>
                            <div class="p-2 bd-highlight"><h1>{{ $user->name }}</h1></div>
                            <div class="p-2 bd-highlight"><h2>Points: {{ $user->points }}</h2></div>
                            <div class="p-2 bd-highlight">
                                <h1>Your Tournaments</h1>
                                @forelse($user->tournaments as $tournament)
                                    <a href="/tournaments/{{ $tournament->id }}">{{ $tournament->name }}</a><br>
                                @empty
                                    <p>You have not signed to any tournaments</p>
                                @endforelse
                            </div>
                        </div>
                    </div>
                    @else
                    <h1>Edit tournaments and set scores</h1>
                    @foreach($tournaments as $tournament)
                        @if($tournament->user_id === Auth::user()->id)
                        <a href="/tournaments/{{ $tournament->id }}">{{ $tournament->name }}</a><br>
                        @endif
                    @endforeach
                    @endif

                    </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
