@extends('layouts.app')

@section('content')
<div class="mt-2 col-md-12"></div>
    <h1>{{$tournament->name}}</h1>
    <p>Start: {{$tournament->start}}</p>
    <p>End: {{$tournament->end}}</p>
    <p>Organizer: {{$tournament->organizer}}</p>
    <p>Place: {{$tournament->place}}</p>
    <p>Maximum Participants: {{$tournament->max_participants}}</p>
    <hr>
    <small>Created at {{$tournament->created_at}}</small>
    <br>
    <br>
    @if(!Auth::guest())
      @if(Auth::user()->id == $tournament->user_id || Auth::user()->group == 'admin')
        <table class="table">
            <tbody>
                <tr>
                    <td><a href="/tournaments/{{$tournament->id}}/edit" class="btn btn-default">Edit Tournament</a><br></td>
                    @if($today > $tournament->start)
                    <td><a href="/tournaments/{{$tournament->id}}/scores/create" class="btn btn-default">Set Scores</a></td>
                    @endif
                    <td>
                        {!!Form::open(['action' => ['TournamentsController@destroy', $tournament->id], 'method' => 'POST', 'class' => 'pull-right'])!!}
                            {{Form::hidden('_method', 'DELETE')}}
                            {{Form::submit('Delete Tournament', ['class' => 'btn btn-danger'])}}
                        {!!Form::close()!!}
                    </td>
                </tr>
            </tbody>
         </table>
      @endif

      @if(Auth::user()->group == 'user')
            @if($today < $tournament->start)
                @if(!in_array(Auth::user()->id, $participants))
                    {!! Form::open(['action' => ['TournamentsController@participate', $tournament->id], 'method' => 'POST']) !!}
                    {{Form::submit('Participate', ['class' => 'btn btn-success'])}}
                    {!!Form::close()!!}
                @else
                    {!! Form::open(['action' => ['TournamentsController@logOff', $tournament->id], 'method' => 'POST']) !!}
                    {{Form::submit('Log off', ['class' => 'btn btn-danger'])}}
                    {!!Form::close()!!}
                @endif
            @endif
        @endif
        <br>
    @endif

    <h1>Participants</h1>
    <table class="table">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Points</th>
              </tr>
            </thead>
            <tbody>
            @forelse ($tournament->users as $key => $user)

            <tr>
                <th scope="row">{{++$key}}</th>
                <td>{{ $user->name }}</td>
                <td>{{ $user->points }}</td>
              </tr>
            @empty
            <tr><td>No participants yet.</td></tr>
            @endforelse
            </tbody>
    </table>

    <h1>Scores</h1>
    <table class="table">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Winner</th>
                <th scope="col">Loser</th>
                <th scope="col">Score</th>
              </tr>
            </thead>
            <tbody>
            @forelse ($scores as $key => $score)

            <tr>
                <th scope="row">{{++$key}}</th>
                <td>{{ $score->winner }}</td>
                <td>{{ $score->loser }}</td>
                <td>{{ $score->score }}</td>
                @if(!Auth::guest())
                    @if(Auth::user()->id == $tournament->user_id || Auth::user()->group === 'admin')
                    <td><a href="/tournaments/{{$tournament->id}}/scores/{{ $score->score_id }}/edit" class="btn btn-default">Edit</a></td>
                    <td>
                        {!!Form::open(['action' => ['ScoresController@destroy', $tournament->id, $score->score_id], 'method' => 'POST', 'class' => 'pull-right'])!!}
                        {{Form::hidden('_method', 'DELETE')}}
                        {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}
                        {!!Form::close()!!}
                    </td>
                    @endif
                @endif
              </tr>
            @empty
              <tr><td>No scores yet.</td></tr>
            @endforelse
            </tbody>
    </table>
@endsection
