@extends('layouts.app')

@section('content')
<div class="mt-2 col-md-12"></div>
    <h1>Edit Tournament</h1>
    {!! Form::open(['action' => ['TournamentsController@update', $tournament->id], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
        <div class="form-group">
          {{Form::label('name', 'Name')}}
          {{Form::text('name', $tournament->name, ['class' => 'form-control', 'placeholder' => 'Name'])}}
        </div>
        <div class="form-group">
          {{Form::label('start', 'Start')}}
          {{Form::date('start', $tournament->start, ['class' => 'form-control', 'placeholder' => 'Start'])}}
        </div>
        <div class="form-group">
          {{Form::label('end', 'End')}}
          {{Form::date('end', $tournament->end, ['class' => 'form-control', 'placeholder' => 'End'])}}
        </div>
        <div class="form-group">
          {{Form::label('place', 'Place')}}
          {{Form::text('place', $tournament->place, ['class' => 'form-control', 'placeholder' => 'Place'])}}
        </div>
        <div class="form-group">
          {{Form::label('max', 'Max')}}
          {{Form::number('max', $tournament->max_participants, ['class' => 'form-control', 'placeholder' => 'Max'])}}
        </div>
        {{Form::hidden('_method', 'PUT')}}
        {{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
    {!! Form::close() !!}
@endsection
