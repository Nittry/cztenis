@extends('layouts.app')

@section('content')
<div class="mt-2 col-md-12"></div>
    <h1>Tournaments</h1>
    <div class="mt-3 col-md-12"></div>
    @if((count($tournaments) > 0))

    <h2>Upcoming</h2>
      {{-- Getting all upcoming tournaments through loop --}}
      @foreach($tournaments as $tournament)
        @if($today < $tournament->start)
            <div class="well">
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <h3><a href="/tournaments/{{$tournament->id}}">{{$tournament->name}}</a></h3>
                        <p>Start: {{ $tournament->start }}</p>
                    </div>
                </div>
            </div>
        @endif
      @endforeach

    <h2>Carried out</h2>
    {{-- Getting all carried out tournaments through loop --}}
        @foreach($tournaments as $tournament)
          @if($today > $tournament->start)
              <div class="well">
                  <div class="row">
                      <div class="col-md-4 col-sm-4">
                          <h3><a href="/tournaments/{{$tournament->id}}">{{$tournament->name}}</a></h3>
                          <p>Start: {{ $tournament->start }}</p>
                      </div>
                  </div>
              </div>
          @endif
        @endforeach

    @else
      <p>No tournaments found</p>
    @endif
@endsection
