<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Main Page Route
Route::get('/', function () {
    return view('welcome');
});

//Auth routes
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

//TournamentsController routes
Route::resource('tournaments', 'TournamentsController');
Route::post('new-participant/{id}', array('uses' => 'TournamentsController@participate'));
Route::post('del-participant/{id}', array('uses' => 'TournamentsController@logOff'));

//UsersController routes
Route::resource('users', 'UsersController');
Route::get('/users/{id}/points', 'UsersController@assignPoints');
Route::put('pointsUpdate/{id}', array('uses' => 'UsersController@pointsUpdate'));
Route::get('new-search', array('uses' => 'UsersController@search'));

//ScoresController routes
Route::get('/tournaments/{id}/scores/create', array('uses' => 'ScoresController@create'));
Route::get('/tournaments/{tournament_id}/scores/{score_id}/edit', array('uses' => 'ScoresController@edit'));
Route::delete('/tournaments/{tournament_id}/scores/{score_id}/destroy', array('uses' => 'ScoresController@destroy'));
Route::post('/tournaments/{id}/scores/add-score', array('uses' => 'ScoresController@store'));
Route::put('/tournaments/{tournament_id}/scores/{score_id}/update', array('uses' => 'ScoresController@update'));

