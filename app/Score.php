<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Score extends Model
{
    //Table Name
    protected $table = 'scores';
    // Primary Key
    public $primaryKey = 'id';
}
