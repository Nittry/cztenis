<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tournament;
use App\User;
use App\Score;
use DB;

class ScoresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        if (auth()->user()->group === 'user') {
            return redirect('/tournaments')->with('error', 'Unauthorized page');
        }

        $tournament = Tournament::find($id);
        $users = $tournament->users->pluck('name', 'id');
        $data = [
            'tournament' => $tournament,
            'users' => $users
        ];
        return view('scores.create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $this->validate($request, [
            'winner' => 'required',
            'loser' => 'required',
            'score' => 'required',
          ]);

          $tournament = Tournament::find($id);
          $winner_id = $request->input('winner');
          $loser_id = $request->input('loser');
          $score = $request->input('score');
          $winner = User::find($winner_id);
          $loser = User::find($loser_id);
          $winnerPoints = $winner->points + $loser->points;
          if ($winner_id !== $loser_id) {
            DB::table('scores')->insert([
            'tournament_id' => $tournament->id,
            'winner_id' => $winner_id,
            'loser_id' => $loser_id,
            'score' => $score,
            'points_gained' => $loser->points
            ]);
            DB::table('users')->where('id', $winner_id)->update(['points' => $winnerPoints]);
          } else {
            return redirect('/tournaments')->with('error', 'Winner cannot be a loser too!');
          }

          return redirect('/tournaments')->with('success', 'Score added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($tournament_id, $score_id)
    {
        if (auth()->user()->group === 'user') {
            return redirect('/tournaments')->with('error', 'Unauthorized page');
        }

        $tournament = Tournament::find($tournament_id);
        $users = $tournament->users->pluck('name', 'id');
        $scores = DB::table('scores as s')
                ->select('s.id as score_id', 'u.id as winner_id', 'v.id as loser_id', 's.score as score')
                ->join('users as u', 'u.id', '=', 's.winner_id' )
                ->join('users as v', 'v.id', '=', 's.loser_id' )
                ->where('tournament_id', $tournament->id)->where('s.id', $score_id)->get();
        $data = [
            'tournament' => $tournament,
            'users' => $users,
            'scores' => $scores,
            'score_id' => $score_id
        ];
        return view('scores.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, $score_id)
    {
        $this->validate($request, [
            'winner' => 'required',
            'loser' => 'required',
            'score' => 'required',
          ]);

            $winner_id = $request->input('winner');
            $loser_id = $request->input('loser');
            $score = $request->input('score');

          if ($winner_id !== $loser_id) {

            $tournament = Tournament::find($id);
            $scoreInfo = Score::find($score_id);
            $lastWinner = User::find($scoreInfo->winner_id);


            //Delete points from the previous winner
            $deletedPoints = $lastWinner->points - $scoreInfo->points_gained;
            DB::table('users')->where('id', $scoreInfo->winner_id)->update(['points' => $deletedPoints]);
            //Add points to the new winner
            $winner = User::find($winner_id);
            $loser = User::find($loser_id);
            $winnerPoints = $winner->points + $loser->points;
            DB::table('users')->where('id', $winner_id)->update(['points' => $winnerPoints]);
            //Add score
            DB::table('scores')->where('id', $score_id)->update([
                    'tournament_id' => $tournament->id,
                    'winner_id' => $winner_id,
                    'loser_id' => $loser_id,
                    'score' => $score,
                    'points_gained' => $loser->points
            ]);

          } else {
            return redirect('/tournaments')->with('error', 'Winner cannot be loser too!');
          }

          return redirect('/tournaments')->with('success', 'Score Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($tournament_id, $score_id)
    {
        $scoreInfo = Score::find($score_id);
        $winner = User::find($scoreInfo->winner_id);
        $pointsToUpdate = $winner->points - $scoreInfo->points_gained;
        DB::table('users')->where('id', $scoreInfo->winner_id)->update(['points' => $pointsToUpdate]);
        DB::table('scores')->where('id', '=', $score_id)->delete();
        return redirect('/tournaments')->with('success', 'Score Deleted and winner points adjusted.');
    }
}
