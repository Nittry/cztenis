<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Tournament;
use App\User;
use App\Score;
use DB;
use DateTime;

class TournamentsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $today = date('Y-m-d');
        $tournaments = Tournament::orderBy('start', 'desc')->get();
        $data = [
            'tournaments' => $tournaments,
            'today' => $today
        ];
        return view('tournaments.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (auth()->user()->group === 'user') {
            return redirect('/users')->with('error', 'Unauthorized page');
        }

        return view('tournaments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
          'name' => 'required',
          'start' => 'required|date|date_format:Y-m-d|before:end',
          'end' => 'required|date|date_format:Y-m-d|after:start',
          'place' => 'required',
          'max' => 'required|numeric|min:2|max:64'
        ]);


        //Create Tournament
        $tournament = new Tournament;
        $tournament->name = $request->input('name');
        $tournament->start = $request->input('start');
        $tournament->end = $request->input('end');
        $tournament->organizer = auth()->user()->name;
        $tournament->place = $request->input('place');
        $tournament->max_participants = $request->input('max');
        $tournament->user_id = auth()->user()->id;
        $tournament->save();

        return redirect('/tournaments')->with('success', 'Tournament Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
            $today = date('Y-m-d');
            $tournament =  Tournament::find($id);
            $participants = [];
            $scores = DB::table('scores as s')
                ->select('s.id as score_id', 'u.name as winner', 'v.name as loser', 's.score as score')
                ->join('users as u', 'u.id', '=', 's.winner_id' )
                ->join('users as v', 'v.id', '=', 's.loser_id' )
                ->where('tournament_id', $tournament->id)->get();
            foreach ($tournament->users as $user) {
                array_push($participants, $user->id);

            }
            $data = [
                'tournament' => $tournament,
                'participants' => $participants,
                'today' => $today,
                'scores' => $scores
            ];

        return view('tournaments.show')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (auth()->user()->group === 'user') {
            return redirect('/users')->with('error', 'Unauthorized page');
        }

        $tournament =  Tournament::find($id);

        // Check for correct user
        if(auth()->user()->group === 'manager' && auth()->user()->id !== $tournament->user_id){
          return redirect('/tournaments')->with('error', 'Unauthorized Page');
        }
        return view('tournaments.edit')->with('tournament', $tournament);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'start' => 'required|date|date_format:Y-m-d|before:end',
            'end' => 'required|date|date_format:Y-m-d|after:start',
            'place' => 'required',
            'max' => 'required|digits_between:2,100'
          ]);


        //Update Tournament
        $tournament = Tournament::find($id);
        $tournament->name = $request->input('name');
        $tournament->start = $request->input('start');
        $tournament->end = $request->input('end');
        $tournament->organizer = auth()->user()->name;
        $tournament->place = $request->input('place');
        $tournament->max_participants = $request->input('max');
        $tournament->user_id = auth()->user()->id;
        $tournament->save();

        return redirect('/tournaments')->with('success', 'Tournament Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (auth()->user()->group === 'user') {
            return redirect('/users')->with('error', 'Unauthorized page');
        }
        $tournament = Tournament::find($id);
        // Check for correct user
        if(auth()->user()->group === 'manager' && auth()->user()->id !== $tournament->user_id){
          return redirect('/tournaments')->with('error', 'Unauthorized Page');
        }
        $tournament->delete();
        DB::table('scores')->where('tournament_id', $tournament->id)->delete();
        return redirect('/tournaments')->with('success', 'Tournament with its scores deleted');
    }

    public function participate($id){
        $tournament = Tournament::find($id);
        $user = Auth::user()->id;
        DB::table('tournament_participants')->insert(['user_id' => $user, 'tournament_id' => $tournament->id]);
        return redirect('/tournaments')->with('success', 'You logged in a tournament');

    }

    public function logOff($id){
        $tournament = Tournament::find($id);
        $user = Auth::user()->id;
        DB::table('tournament_participants')->where('user_id', $user)->where('tournament_id', $tournament->id)->delete();
        return redirect('/tournaments')->with('success', 'You logged off a tournament');
    }
}
