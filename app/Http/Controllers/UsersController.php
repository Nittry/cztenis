<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\User;
use DB;


class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show', 'search']]);
    }

    public function index()
    {
        $users = User::orderBy('points','desc')->where('group', 'user')->get();
        $allUsers = User::orderBy('name','asc')->get();
        $data = [
            'users' => $users,
            'allUsers' => $allUsers
        ];
        return view('users.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (auth()->user()->group === 'user') {
            return redirect('/users')->with('error', 'Unauthorized page');
        }
        else{
            return redirect('/users')->with('error', 'Page does not exist');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   $user = User::find($id);
        return view('users.show')->with('user', $user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $groups = User::distinct('group')->pluck('group', 'group');
        $data = [
            'user' => $user,
            'groups' => $groups
        ];
        if (auth()->user()->group === 'admin') {
            return view('users.edit')->with($data);
        } else {
            if (auth()->user()->id !== $user->id) {
                return redirect('/users')->with('error', 'Unauthorized page');
            }
        }
        return view('users.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

            if ($request->input('password') !== $request->input('password_confirmation')) {
                return redirect('/home')->with('error', 'Passwords do not match!');
            }

            $user = User::find($id);

          //Update User
          if (auth()->user()->id === $user->id) {

            $this->validate($request, [
                'name' => 'required',
                'email' => 'unique:users,email,'.Auth::id(). 'required|email',
                'password' => 'required',
                'profile_image' => 'image|nullable|mimes:jpeg,png,jpg,gif,svg|max:2048'
              ]);

            //Hande file upload
            if($request->hasFile('profile_image')){
                $fileNameWithExt = $request->file('profile_image')->getClientOriginalName();
                // Get just filename
                $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
                //Get just ext
                $extension = $request->file('profile_image')->getClientOriginalExtension();
                //File name to store
                $fileNameToStore = $filename.'_'.time().'.'.$extension;
                //Upload Image
                $path = $request->file('profile_image')->storeAs('public/profile_images', $fileNameToStore);

                if ($user->profile_image === 'default.png') {
                    $user->profile_image = $fileNameToStore;
                } else {
                    //DELETE Image
                Storage::delete('public/profile_images/'.$user->profile_image);
                $user->profile_image = $fileNameToStore;
                }
            }

            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->password = bcrypt($request->input('password'));
            $user->save();

          }
          else{

            $this->validate($request, [
                'group' => 'required',
              ]);

            $user->group = $request->input('group');
            $user->save();
          }

          return redirect('/home')->with('success', 'Profile Updated');



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return redirect('/users')->with('error', 'Page does not exist');
    }

    public function pointsUpdate(Request $request, $id)
    {
        $this->validate($request, [
            'points' => 'required'
          ]);


          //Update User
          $user = User::find($id);
          $user->points =  $request->input('points');
          $user->save();

          return redirect('/users')->with('success', 'Points assigned');
    }

    public function assignPoints($id)
    {
        if (auth()->user()->group === 'user') {
            return redirect('/users')->with('error', 'Unauthorized page');
        }

        $user = User::find($id);
        return view('users.assignPoints')->with('user', $user);
    }

    public function search(Request $request)
    {
        $this->validate($request, [
            'search' => 'required'
          ]);

        $keyword =  $request->input('search');
        $users = DB::table('users')->where("name", "LIKE","%$keyword%")
        ->orWhere("points", "LIKE", "%$keyword%")->get();

          return view('users.search')->with('users', $users);
    }
}
