<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tournament extends Model
{
    //Table Name
    protected $table = 'tournaments';
    // Primary Key
    public $primaryKey = 'id';
    //Timestamps
    public $timestamps = true;

    public function users(){
      return $this->belongsToMany('App\User', 'tournament_participants');
    }
}
